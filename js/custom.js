	// Variables
	var REFRESH_RATE 	  = 25;
	var PLAYGROUND_WIDTH  = 708;
	var PLAYGROUND_HEIGHT = 384;
	var BG_SPRITE_HEIGHT  = 128;
	var PLAYER_WIDTH	  = 43;
	var PLAYER_HEIGHT	  = 48;

	var background1 	  = new $.gQ.Animation({imageURL: "img/stands.png"});
	var background2 	  = new $.gQ.Animation({imageURL: "img/stands2.png"});
	var midground		  = new $.gQ.Animation({imageURL: "img/grass.png"});
	var track 			  = new $.gQ.Animation({imageURL: "img/track.png"});
	var player 			  = new $.gQ.Animation({imageURL: "img/test.png"});
	var collided		  = new Array();
	var isColliding		  = false;
	
	// Gloabl animation holder
	var playerAnimation   = new Array();
	var ramps			  = new Array();
 	
	playerAnimation["upLane"]    = new $.gQ.Animation({imageURL: "img/upLane_iso.png"});
	playerAnimation["downLane"]  = new $.gQ.Animation({imageURL: "img/downLane_iso.png"});
	playerAnimation["stopped"]   = new $.gQ.Animation({imageURL: "img/stopped.png"});
	ramps["smallUp"]			 = new $.gQ.Animation({imageURL: "img/small-ramp-up.png"});
	ramps["trigger"]			 = new $.gQ.Animation();
	
	var trackSpeed		  = 0;
	var standsSpeed		  = 0;
	var maxSpeed		  = 10; // watch it, buddy
	
	
	function shake(){
    $('#player').animate({
	    'top': 0,
        'top': '-=1px',
        'top': '+=1px'
    }, 30, function() {
        $('#player').animate({
            'top': '+=1px',
            'top': '-=1px'
        }, 30);	
	  }
	)};
	
	function jump(){
		isColliding = true;
		console.log("bip!");
		setTimeout(function() {
			isColliding = false;	
		}, 1000);
	};

	// Instantiate the 'playground' or canvas-like element
	$("#playground").playground({height: PLAYGROUND_HEIGHT, width: PLAYGROUND_WIDTH});

	// Create Groups and add Sprites to those Groups
    $.playground().addGroup("background", {width: PLAYGROUND_WIDTH, height: BG_SPRITE_HEIGHT})
	           .addSprite("background1", {animation: background1,
	                       width: PLAYGROUND_WIDTH, height: BG_SPRITE_HEIGHT})
	           .addSprite("background2", {animation: background2,
	                       width: PLAYGROUND_WIDTH, height: BG_SPRITE_HEIGHT,
	                       posx: PLAYGROUND_WIDTH})
	           .end()
	           .addGroup("midground", {width: PLAYGROUND_WIDTH, height: BG_SPRITE_HEIGHT})
	           		.addSprite("midground1", {animation: midground,
	           			   width: PLAYGROUND_WIDTH, height: BG_SPRITE_HEIGHT})
	           		.addSprite("midground2", {animation: midground,
	           			   width: PLAYGROUND_WIDTH, height: BG_SPRITE_HEIGHT,
	           			   posx: PLAYGROUND_WIDTH})
	           .end()
	           .addGroup("track", {width: PLAYGROUND_WIDTH, height: BG_SPRITE_HEIGHT})
	           		.addSprite("track1", {animation: track,
	           			   width: PLAYGROUND_WIDTH, height: BG_SPRITE_HEIGHT})
	           		.addSprite("track2", {animation: track,
	           			   width: PLAYGROUND_WIDTH, height: BG_SPRITE_HEIGHT,
	           			   posx: PLAYGROUND_WIDTH})
	           	.end()
	           	.addGroup("ramps", {width: PLAYGROUND_WIDTH, height: 256})
	           		.addSprite("smallRampUp", {animation: ramps["smallUp"],
	           				width: 100, height: 137,
	           				posx: 500, posy: 215})
	           		.addSprite("rampTrigger", {animation: ramps["trigger"],
		           			width: 1, height: 137,
		           			posx: 499, posy: 215})
	           	.end()
	           	.addGroup("player", {posx: PLAYGROUND_WIDTH/2 - 20, posy: 275, width: PLAYER_WIDTH, height: PLAYER_HEIGHT})
	           		.addSprite("playerIdle", {animation: player,
		           			width: PLAYER_WIDTH, height: PLAYER_HEIGHT,
		           			posx: 0, posy: 0 })
		           	.addSprite("playerUpLane", {animation: player["upLane"], posx: 0, posy: 0, width: 43, height: 47} )
		           	.addSprite("playerDownLane", {animation: player["downLane"], posx: 0, posy: 0, width: 43, height: 47} )
	           	.end();

	// Get ta goin.
	$.playground().startGame();

	// Background Animation
	$.playground().registerCallback(function(){

          //Offset all panes:
          var newPos = ($("#background1").x() - standsSpeed - PLAYGROUND_WIDTH) % (-2 * PLAYGROUND_WIDTH) + PLAYGROUND_WIDTH;
          $("#background1").x(newPos);

          var newPos = ($("#background2").x() - standsSpeed - PLAYGROUND_WIDTH) % (-2 * PLAYGROUND_WIDTH) + PLAYGROUND_WIDTH;
          $("#background2").x(newPos);

          var newPos = ($("#track1").x() - trackSpeed - PLAYGROUND_WIDTH) % (-2 * PLAYGROUND_WIDTH) + PLAYGROUND_WIDTH;
          $("#track1").x(newPos);

          var newPos = ($("#track2").x() - trackSpeed - PLAYGROUND_WIDTH) % (-2 * PLAYGROUND_WIDTH) + PLAYGROUND_WIDTH;
          $("#track2").x(newPos);
          
          var newPos = ($("#smallRampUp").x() - trackSpeed);
          $("#smallRampUp").x(newPos);
          $("#rampTrigger").x(newPos);
		
	  // this essentially ensures we don't check for collisions when an animation is happening
      if (!isColliding) {          
	    // check for collisions with one of the player sprites
	    // collision() returns an array of objects the selector is currently colliding with
	    collided = $('#playerDownLane').collision(); 
	  
	    // if ramp trigger is in that array, jump baby jump!
	    // apparently inArray can find jQuery objects by ID
	    if ( $.inArray(rampTrigger, collided) !== -1 ) {
	      jump();
	    } 
	  }
          
         
      // if you stop put a paw down for crying out loud
      if (trackSpeed == 0) {
	    $('#playerIdle').setAnimation(playerAnimation["stopped"]);
	  }
    }, REFRESH_RATE);
    


    //this is where the key-down binding occurs
	$(document).keydown(function(e){
			switch(e.keyCode){
				case 65: //this is the 'A' key
						if(trackSpeed < maxSpeed) {
							trackSpeed++;
							standsSpeed	= trackSpeed/2;
							$('#playerIdle').setAnimation(player);
						} 
				break;
				case 83: //this is the 'S' key
						if(trackSpeed > 0) {
							trackSpeed--;
							standsSpeed	= trackSpeed/2;	
						} 
				break;
				case 38: //this is the down arrow
					if(trackSpeed > 0) {	
						$('#playerIdle').setAnimation();
						$("#playerUpLane").setAnimation(playerAnimation["upLane"]);
					}
				break;
				case 40: //this is the down arrow 
					if(trackSpeed > 0) {	
						$('#playerIdle').setAnimation();
						$("#playerDownLane").setAnimation(playerAnimation["downLane"]);
					}
				break;
			}
	});
	
	 //this is where the key-up binding occurs
	$(document).keyup(function(e){
			switch(e.keyCode){
				case 38: //this is the up arrow 
					$("#playerUpLane").setAnimation();
					$('#playerIdle').setAnimation(player);
				break;
				case 40: //this is the down arrow 
					$("#playerDownLane").setAnimation();
					$('#playerIdle').setAnimation(player);
				break;
			}
	});

    
    
    
//      setInterval(shake, 105);
